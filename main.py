import math
import os

import arcade

VELOCITY_PLAYER = 300
SPRITE_SIZE = 25
SPRITE_SCALING_PLAYER = 0.5

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Starting Template"
PLAYER_HIT_RADIUS = 20

PLAYER_VALUES = [
    {
        "sprite": "images/playerShip1_green.png",
        "start_x": 20,
        "start_y": 20,
    },
    {
        "sprite": "images/playerShip1_blue.png",
        "start_x": SCREEN_WIDTH - 20,
        "start_y": SCREEN_HEIGHT - 20,
    },
]

class AnarchyGame(arcade.Window):
    """
    Main application class.

    NOTE: Go ahead and delete the methods you don't need.
    If you do need a method, delete the 'pass' and replace it
    with your own code. Don't leave 'pass' in this program.
    """

    def __init__(self, width, height, title):
        super().__init__(width, height, title)

        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        self.player_list = None
        self.player_sprite = []
        
        self.player_x = []
        self.player_y = []
        self.player_vx = []
        self.player_vy = []
        self.player_angle= []

        self.bullets = None
        self.bullet_index = 0
        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):

        self.player_list = arcade.SpriteList()
        self.player_sprite = []
        
        self.player_x = []
        self.player_y = []
        self.player_vx = []
        self.player_vy = []
        self.player_angle= []

        for values in PLAYER_VALUES:
            
            self.player_x.append(values["start_x"])
            self.player_y.append(values["start_y"])
            self.player_vx.append(VELOCITY_PLAYER)
            self.player_vy.append(0)
            self.player_angle.append(0)

            sprite = arcade.Sprite(values["sprite"] , SPRITE_SCALING_PLAYER)
            self.player_sprite.append(sprite)
            self.player_list.append(sprite)
            sprite.center_x = values["start_x"]
            sprite.center_y = values["start_y"]
            sprite.angle = - 90

        self.bullets = []
        for i in range(100):
            self.bullets.append([-10, -10, 0, 0])
        self.bullet_index = 0


    def on_draw(self):
        """
        Render the screen.
        """

        # This command should happen before we start drawing. It will clear
        # the screen to the background color, and erase what we drew last frame.
        arcade.start_render()

        
        for i in range (len(self.player_sprite)):
            self.player_sprite[i].center_x = self.player_x[i]
            self.player_sprite[i].center_y = self.player_y[i]

        self.player_list.draw()
        #arcade.draw_text("Bla", 10, 20, arcade.color.WHITE, 14)

        arcade.draw_points(self.bullets, arcade.color.RED_DEVIL, 4)

    def update(self, delta_time):

        for i in range(len(self.player_x)):
            self.player_x[i] += self.player_vx[i] * delta_time
            self.player_y[i] += self.player_vy[i] * delta_time

            if self.player_x[i] > SCREEN_WIDTH - SPRITE_SIZE:
                self.player_x[i] = SCREEN_WIDTH - SPRITE_SIZE
            if self.player_x[i] < SPRITE_SIZE:
                self.player_x[i] = SPRITE_SIZE

            if self.player_y[i] > SCREEN_HEIGHT - SPRITE_SIZE:
                self.player_y[i] = SCREEN_HEIGHT - SPRITE_SIZE
            if self.player_y[i] < SPRITE_SIZE:
                self.player_y[i] = SPRITE_SIZE

        for i in range(100):
            bullet = self.bullets[i]
            bullet[0] += bullet[2] * delta_time
            bullet[1] += bullet[3] * delta_time

            for j in range(len(self.player_x)):
                dx = self.player_x[j] - self.bullets[i][0]
                dy = self.player_y[j] - self.bullets[i][1]
                d = math.sqrt(dx*dx + dy*dy)

                if d<PLAYER_HIT_RADIUS:
                    self.bullets[i][0] = -100
                    self.bullets[i][2] = 0





    def rotate_player(self, angle, player_number):
        self.player_angle[player_number] += angle + 360
        self.player_angle[player_number] %= 360

        self.player_sprite[player_number].angle = self.player_angle[player_number] - 90

        self.player_vx[player_number] = math.cos(self.player_angle[player_number] * math.pi / 180) * VELOCITY_PLAYER
        self.player_vy[player_number] = math.sin(self.player_angle[player_number] * math.pi / 180) * VELOCITY_PLAYER

    def shoot(self, player_number):
        bullet =  self.bullets[self.bullet_index]
        bullet[0] = self.player_x[player_number] + self.player_vx[player_number] / VELOCITY_PLAYER * PLAYER_HIT_RADIUS
        bullet[1] = self.player_y[player_number] + self.player_vy[player_number] / VELOCITY_PLAYER * PLAYER_HIT_RADIUS
        bullet[2] = self.player_vx[player_number] * 2
        bullet[3] = self.player_vy[player_number] * 2
        self.bullet_index += 1
        self.bullet_index %=100

    def on_key_press(self, key, key_modifiers):

        if key == arcade.key.Y:
            self.rotate_player(45, 0)
        elif key == arcade.key.X:
            self.rotate_player(-45, 0) 
        elif key == arcade.key.Q:
            self.shoot(0)

        if key == arcade.key.N:
            self.rotate_player(45, 1)
        elif key == arcade.key.M:
            self.rotate_player(-45, 1)
        elif key == arcade.key.O:
            self.shoot(1)

def main():
    """ Main method """
    game = AnarchyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
